package com.payment.resource;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.payment.constant.Message;
import com.payment.exception.BadRequestException;
import com.payment.model.Wallet;
import com.payment.service.WalletService;
import com.payment.validation.CustomeValidation;

@Path("/v1/accounts")
public class WalletResource {

	@Inject
	WalletService walletService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllAccounts() {
		
		List<Wallet> wallets = walletService.allWallet();
		return Response.ok(wallets).build();
	}

	@POST
	@Path("/account")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createAccount(Wallet wallet) {

		CustomeValidation.validateWallet(wallet);
	    
		wallet = walletService.addWallet(wallet);
		
		return Response.status(Status.CREATED).entity(wallet).build();
	}

	@PUT
	@Path("/account/addmoney")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addMoney(Wallet wallet) {

		CustomeValidation.validateWallet(wallet);
		
		wallet = walletService.addMoney(wallet);

		return Response.status(Status.CREATED).entity(wallet).build();

	}
	
	
}