package com.payment;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

import org.junit.jupiter.api.Test;

import com.payment.model.Wallet;
import com.payment.repository.Repository;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import static com.payment.constant.ValidationMessage.AMOUNT_ERROR;
import static com.payment.constant.ValidationMessage.MOBILE_VALIDATION;;

@QuarkusTest
public class WalletResourceTest {

	@Inject
	Repository repository;
	
    @Test
    public void testWalletNotAvailable() {
        given()
          .when().get("/v1/accounts")
          .then()
             .statusCode(200)
             .body(is("[]"));
    }
    
    @Test
    public void testAllWalletAvaiable() {
    	
    	loadWallet1();
    	
        given()
          .when().get("/v1/accounts")
          .then()
             .statusCode(200);
//             .body("size()", is(1))
//             .and()
//             .body("[0].phoneNumber", is("9832799827"))
//             .and()
//             .body("[0].balance", is(1000));
    }
    
    @Test
    public void testAddWallet() {
    	
        given()
          .when()
          .accept(ContentType.JSON)
          .contentType(ContentType.JSON)
          .body(loadWallet2())
          .post("/v1/accounts/account")
          .then()
             .statusCode(201);
    }
    
    @Test
    public void testAddWallet_BAD_REQUEST_PHONE_NON_NUMERIC() {
    	
        given()
          .when()
          .accept(ContentType.JSON)
          .contentType(ContentType.JSON)
          .body("{\"phoneNumber\": \"9832799828SS\",\"balance\": 1000}")
          .post("/v1/accounts/account")
          .then()
             .statusCode(400)
             .body("errorMessage", containsString(MOBILE_VALIDATION));
    }
    
    @Test
    public void testAddWallet_BAD_REQUEST_PHONE_LESS_10() {
    	
        given()
          .when()
          .accept(ContentType.JSON)
          .contentType(ContentType.JSON)
          .body("{\"phoneNumber\": \"983279982\",\"balance\": 1000}")
          .post("/v1/accounts/account")
          .then()
             .statusCode(400)
             .body("errorMessage", containsString(MOBILE_VALIDATION));
    }
    
    @Test
    public void testAddWallet_BAD_REQUEST_PHONE_GREATER_10() {
    	
        given()
          .when()
          .accept(ContentType.JSON)
          .contentType(ContentType.JSON)
          .body("{\"phoneNumber\": \"983279982233\",\"balance\": 1000}")
          .post("/v1/accounts/account")
          .then()
             .statusCode(400)
             .body("errorMessage", containsString(MOBILE_VALIDATION));
    }
    
    @Test
    public void testAddWallet_BAD_REQUEST_BALANCE_ZERO() {
    	
        given()
          .when()
          .accept(ContentType.JSON)
          .contentType(ContentType.JSON)
          .body("{\"phoneNumber\": \"9832799829\",\"balance\": 0}")
          .post("/v1/accounts/account")
          .then()
             .statusCode(400)
             .body("errorMessage", containsString(AMOUNT_ERROR));
    }
    
    @Test
    public void testAddWallet_BAD_REQUEST_BALANCE_NEGATIVE() {
    	
        given()
          .when()
          .accept(ContentType.JSON)
          .contentType(ContentType.JSON)
          .body("{\"phoneNumber\": \"9832799829\",\"balance\": -1}")
          .post("/v1/accounts/account")
          .then()
             .statusCode(400)
             .body("errorMessage", containsString(AMOUNT_ERROR));
    }
    
	private void loadWallet1() {
		Wallet w1 = new Wallet();
		w1.setPhoneNumber("9832799827");
		w1.setBalance(new BigDecimal(1000));

		repository.addAccount(w1);
	}
	
	private String loadWallet2() {
    	String s = "{\n" + 
    			"	\"phoneNumber\": \"9832799828\",\n" + 
    			"	\"balance\": 1000\n" + 
    			"}";
    	return s;
    }

}